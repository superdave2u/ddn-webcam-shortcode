<?php
/*
	Plugin Name: DDN Live Webcam Shortcode
	Plugin URI: http://webmanna.com
	Author: Dave Mainville
	Author URI: http://superdave2u.com
	Description: Wordpress Shortcode to pull Dog Days webcam feed <em>[ddn_cam number="x" delay="y" width="z"]</em>
	
	Static Feed 		- [ddn_cam number="x"]
	Live Feed 			- [ddn_cam number="x" delay="y"]
	Feed w Set Width	- [ddn_cam number="x" delay="y" width="z"]
*/
class WM_Live_Webfeed_Shortcode {
	
	var $feedBase        	= 'http://dogdays.viewmydog.com/';
	var $camBase         	= 'cam';
	var $camSuffix       	= '.jpg';
	
	var $injectController	= false;
	
	function __construct() {
		// add shortcode to WP
		add_shortcode('ddn_cam',array($this,'shortcode_func'));
		add_action('wp_enqueue_script',array($this,'script_init'));
		add_action('wp_footer',array($this, 'inject_controller'));
	}

	function script_init() {
		wp_enqueue_script('jquery');
	}
	
	function inject_controller() {
		if ( ! $this->injectController )
			return; // only inject code if shortcode is used on page
			
		$output = '
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$(".live_feed").each(function(){
					var img    	= $(this);
					var baseSrc	= img.attr("src");
					var rate   	= img.data("delay") * 1000;
					var timeout = img.data("timeout") * 60 * 1000;
					
					setInterval(function(){
						var d = new Date();
						img.attr("src", baseSrc + "?" + d.getTime());
					}, rate);
					
					if(timeout > 0) {
						startTimeoutTimer(timeout);
					}
				});
			});
			function startTimeoutTimer(timeoutTimer) {
				setInterval( function(){ 
					if(confirm("Continue watching live video?")) {
						startTimeoutTimer(timeoutTimer);
					} else {
						window.close(); 
					}
				}, timeoutTimer);
			}
		</script>
		';
		
		echo $output;
	}
	
	function shortcode_func($atts) {
		$this->injectController = true;
		
		extract( shortcode_atts( array(
			'number' 	=> 1,
			'delay' 	=> 0,
			'width' 	=> 600,
			'timeout'	=> 0,
		), $atts, 'ddn_cam' ) );

		if ($number > 3)
			return 'Invlaid camera number';
		
		$class = ($delay === 0) ? 'static_feed' : 'live_feed' ;
		
		$feedUrl  = $this->feedBase;
		$feedUrl .= $this->camBase;
		$feedUrl .= $number;
		$feedUrl .= $this->camSuffix;
		
		$ret = '<img';
			$ret .= " src='$feedUrl'";
			$ret .= " data-timeout='$timeout'";
			$ret .= " data-delay='$delay'";
			$ret .= " class='$class'";
			$ret .= ' style="width:'. $width .'px;height:auto;"';
		$ret .= '/>';

		return $ret;
	}
}
$WM_Live_Webfeed_Shortcode = new WM_Live_Webfeed_Shortcode();
?>